import platform
from pathlib import PosixPath, WindowsPath
import pathlib

def whatpathtype():

    if platform.system() == 'Linux':
        typepath = type(PosixPath())

    elif platform.system() == 'Windows':
        temp = pathlib.PosixPath
        pathlib.PosixPath = pathlib.WindowsPath
        typepath = type(WindowsPath())

    return typepath

def whatos():

    return platform.system()