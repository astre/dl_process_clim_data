from datetime import date
import dash
from dash import dcc, html, callback, Output, Input, State
import dash_leaflet as dl
from dash.exceptions import PreventUpdate
import json

dash.register_page(__name__,
                   path='/results',
                   name='Results',
                   title='Results',
                   description='Inputs data for the processing')

EMPTY_GEOJSON = {
    "type": "FeatureCollection",
    "features": []
}

layout = html.Div([
            dl.Map(
                [
                    dl.TileLayer(),
                    dl.GeoJSON(
                        id='geojson-result',
                        style={
                            'weight': 1,
                            'fillColor': 'blue',
                            'color': 'blue',
                            'fillOpacity': 0.5
                        }
                    )
                ],
                id='viewport_result',
                center=[46.0, 2.0],  # Set initial center
                zoom=5,  # Set initial zoom
                style={'width': '100%', 'height': '600px', 'zIndex': 500}
            ),

            html.Div([
                html.H4("Debug Information"),
                html.Div(id='test', style={'marginBottom': '10px'}),
                html.Div(id='test2', style={'marginBottom': '10px'}),
                html.Div(id='test3', style={'marginBottom': '10px'}),
                html.Div(id='test4', style={'marginBottom': '10px'}),

            ], style={'margin': '20px'})
        ]
    )




# Update map with GeoJSON data
@callback(
    [Output('geojson-result', 'data'),
     Output('test', 'children')],
    Input('result-store-geo', 'data'),
    prevent_initial_call=False
)
def update_geojson(geojson_data):
    print(f"Received GeoJSON data: {type(geojson_data)}")
    if geojson_data is None:
        return EMPTY_GEOJSON, "No GeoJSON data available"

    features_count = len(geojson_data.get('features', []))
    print(f"GeoJSON features found: {features_count}")
    status = f"GeoJSON loaded with {features_count} features"
    return geojson_data, status


# Update viewport info display
@callback(
    Output('viewport_result', 'data'),
    Input('test_dict_sharing', 'data'),
    prevent_initial_call=False
)
def show_viewport(viewport):
    if viewport is None:
        return "No viewport data"

    status = f"Viewport info: {viewport}"
    print(f"Viewport info: {viewport}")
    return viewport




#------------------------
# tests------------------

@callback(
    Output('test2', 'children'),
    Input('test_dict_sharing', 'data'),
    prevent_initial_call=False
)
def show_viewport(viewport):
    if viewport is None:
        return "No viewport data"

    status = f"Viewport info: {viewport}"
    print(f"Viewport info: {viewport}")
    return status


@callback(
    Output('test3', 'children'),
    Input('path-to-vector', 'data'),
    prevent_initial_call=False
)
def show_path(path):
    if path is None:
        return "No path data"

    status = f"Vector file path: {path}"
    print(f"Path info: {status}")
    return status

# Display path information
@callback(
    Output('test4', 'children'),
    Input('result-store-geo', 'data'),
    Input('result-store-data', 'data'),
    prevent_initial_call=False
)
def show_path(geo, data):
    if data is None:
        return "no data"

    status = f"data stored: {data, geo}"
    #print(f"data storedo: {status}")
    return status




"""
# GeoJSON vide initial
EMPTY_GEOJSON = {
    "type": "FeatureCollection",
    "features": []
}


# Define the layout
layout = html.Div([
        dbc.Row([
        # Column for the map
        dbc.Col([
            dl.Map(
                [
                    dl.TileLayer(),
                    dl.GeoJSON(
                        id='geojson-layer',
                        data=EMPTY_GEOJSON,
                        style={
                            'weight': 1,
                            'fillColor': 'blue',
                            'color': 'blue',
                            'fillOpacity': 0.5
                        },
                        zoomToBounds=True
                    )
                ],
                id='map',
                center=center,  # Center for France
                zoom=zoom,  # Default zoom level
                style={'width': '100%', 'height': '600px', 'zIndex': 500}  # Set a lower z-index for the map
            ),
        ])
])])
"""
