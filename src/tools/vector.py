import geopandas as gpd
import traceback

def open_vector(pathtovector):

    try:
       vector_in = gpd.read_file(pathtovector)

    except Exception:
       return traceback.print_exc()

    return vector_in

def boundingbox(vectorobject):

    bounds = vectorobject.total_bounds
    bounds_dict = {
        'xmin': bounds[0],
        'ymin': bounds[1],
        'xmax': bounds[2],
        "ymax": bounds[3],
    }
    return bounds_dict

def write_geobject(geobject, path_file):
    """
    @param geobject: geopandas object
    @param path_file: the path to save the geopandas object, e.g. : 'path/name.shp'
    """
    geobject.to_file(path_file)


def transform_projection(geo_df, epsg_code=4326):
    """
    Transforms the CRS of a GeoPandas DataFrame to EPSG:5432.

    Parameters:
    geo_df (gpd.GeoDataFrame): The input GeoPandas DataFrame with a defined CRS.

    Returns:
    gpd.GeoDataFrame: A new GeoPandas DataFrame with the CRS transformed to EPSG:5432.
    """
    # Ensure the input GeoDataFrame has a CRS defined
    if geo_df.crs is None:
        raise ValueError("Input GeoDataFrame does not have a CRS defined.")

    # Transform the CRS to EPSG:5432
    transformed_geo_df = geo_df.to_crs(epsg=epsg_code)

    return transformed_geo_df