import dash
from dash import html, dcc, Input, Output
import dash_bootstrap_components as dbc
from pathlib import Path
import json

app = dash.Dash(__name__, use_pages=True, external_stylesheets=[dbc.themes.SPACELAB], suppress_callback_exceptions=True, update_title=False )
server = app.server


"""
A faire:
Actuellement les geojsons sont stockées non pas dans la session du navigateur mais dans un système de cache.
Ce cache est stocké directement sur l'ordinateur executant l'app.
Pourquoi un cache ? Error du callback avant storage session exceeded et lenture de chargement entre home et inputs par exemple.Error de partage de données avec le stockage dans un dcc session, les navigateurs ne supportent pas autant de données que présent dans certains geojson.
Donc passage de ces éléments dans le cache. 

Les autres éléments sont actuellement stockés dans la session et le resteront car ils permettent de revenir rapidement sur les éléments selectionnés quand changement de page.
PAreillement le cache permet aussi plus de rapidité dans le chargement des données en geojson.

Donc, pour la suite :
- Envoyer les resultats du process non pas dans un dcc.store mais plutot directement dans le cache.
- Supprimer les liens vers les dcc.store dans la page result. ils seront tous recalculés à partir du cache.
- Créer des graph + une carte dynamique avec consultation des valeurs sur leaflet + extraction des infos dans différents formats.
"""


test_value = {'viewport': 'None'}
test_dict_sharing = None

sidebar = dbc.Nav(
            [
                dbc.NavLink(
                    [
                        html.Div(page["name"], className="ms-2"),
                    ],
                    href=page["path"],
                    active="exact",
                )
                for page in dash.page_registry.values()
            ],
            vertical=True,
            pills=True,
            className="bg-light",
)




app.layout = (
html.Div([
    dcc.Store(id='uploaded-data', storage_type='session', data=None),
    dcc.Store(id='shared-viewport', storage_type='session', data=None),  # created and use from inputs, used also in results
    dcc.Store(id='geojson-layer', storage_type='session', data=None),
    dcc.Store(id='path-to-vector', storage_type='session', data=None),
    dcc.Store(id='test_dict_sharing', storage_type='session', data=None ),
    dcc.Store(id='date-store', storage_type='session', data=None ),
    dcc.Store(id='init-date-store', storage_type='session', data=None),
    dcc.Store(id='column-store', storage_type='session', data=None),
    dcc.Store(id='df-columns', storage_type='session', data=None),  # Store for dataframe columns
    dcc.Store(id='selected-column', storage_type='session', data=None),
    dcc.Store(id='dataprovider', storage_type='session', data=None),
    dcc.Store(id='selected-variables', storage_type='session', data=None),
    dcc.Store(id='result-store-geo', storage_type='session', data=None),
    dcc.Store(id='result-store-data', storage_type='session', data=None),



    html.Div(id='dummy-output'),
    dbc.Container([
    dbc.Row([
        dbc.Col(html.Div("Climate Data Download",
                         style={'fontSize':50, 'textAlign':'center'}))
    ]),

    html.Hr(),

    dbc.Row(
        [
            dbc.Col(
                [
                    sidebar
                ], xs=4, sm=4, md=2, lg=2, xl=2, xxl=2),

            dbc.Col(
                [
                    dash.page_container
                ], xs=8, sm=8, md=10, lg=10, xl=10, xxl=10)
        ]
    )

], fluid=True)]))






@app.callback(
    Output('dummy-output', 'children'),
    Input('shared-viewport', 'data'),
    Input('geojson-layer', 'data'),
    Input('path-to-vector', 'data'),
    Input('test_dict_sharing', 'data'),
    Input('result-store-geo', 'data'),
)
def debug_stores(viewport, geojson_layer, path, test_dict_sharing, data):
    print("Store Contents:")
    print("Viewport:", viewport)
    #print("GeoJSON:", geojson_layer)

    print("Path:", path)
    print("test" , test_dict_sharing)
    print("data", data)

    import sys
    print('geojson_layer:', sys.getsizeof(geojson_layer))


    return ""



if __name__ == "__main__":
    file_path = Path('progress.json')
    # Check if the file exists before attempting to delete it
    if file_path.exists():
        progress_store = {
            "total_download": 0, "current_download": 0,
            "calculation": 0, "calculation_total": 0,
            "save_file": 0, "save_total": 0, "processing_name": None
        }
        # update
        with open('progress.json', 'w') as f:

            json.dump(progress_store, f)


    app.run(debug=True, port=8520 , use_reloader=False)





