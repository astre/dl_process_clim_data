import psutil
from src.tools.control_os import whatpathtype
import os

def limit_cpu():

    "is called at every process start"
    p = psutil.Process(os.getpid())
    # set to lowest priority, this is windows only, on Unix use ps.nice(19)
    if whatpathtype() == 'Windows':
        p.nice(psutil.REALTIME_PRIORITY_CLASS)
    if whatpathtype() == 'Linux':
        None