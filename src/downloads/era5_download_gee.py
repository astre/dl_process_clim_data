import ee
import xarray as xr
import xee

def download_gee_xr(variables, starttime, endtime, bbox_vector) :
    ee.Initialize(opt_url='https://earthengine-highvolume.googleapis.com')

    gee_dict = {}

    for var in variables :

        if var == 't2m':
            var = 'temperature_2m'
        elif var == 'tp':
            var = 'total_precipitation_sum'
        else:
            raise AttributeError(print('Name of variable not correct'))

        collectionGeometry = ee.Geometry.Rectangle(bbox_vector['xmin'], bbox_vector['ymin'], bbox_vector['xmax'], bbox_vector['ymax'])

        ic = ee.ImageCollection('ECMWF/ERA5_LAND/DAILY_AGGR').select(var).filterDate(starttime, endtime)

        # Check if the ImageCollection is empty
        if ic.size().getInfo() == 0:
            raise ValueError(f"No images found for variable '{var}' in the specified date range and bounding box.")

        # Print the first image info for debugging
        first_image = ic.first()
        print(f"First image info: {first_image.getInfo()}")

        dataset = xr.open_dataset(ic, engine='ee', geometry=collectionGeometry, projection=ic.first().select(0).projection(), scale=0.1)

        gee_dict[var] = dataset.transpose("time", "lat", "lon")

    return gee_dict


#