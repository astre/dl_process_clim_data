import pandas as pd
from pathlib import Path

def save_txt_default(vector_path, outrearrange, stats):

    name = Path(vector_path).name
    name_compose = f"{stats}_{name}_meteofranceform"
    path = Path(vector_path).parent

    outrearrange.to_csv(f"{path}/{name_compose}.txt")

    return print("TXT product save")


def meteofrance_rearange_output(geobject, vector_id, variable):

    if variable == 'tp' or variable == 'temperature_2m':
        var_out = 'RR'
    elif variable == 't2m' or variable == 'total_precipitation_sum':
        var_out = 'TP'

    #Rearange data for meteo france data source comparison
    date_list = list(geobject.keys().drop(['geometry', vector_id, 'var']))
    geobject_rearrange = pd.DataFrame(columns=['ID', 'DATE', var_out])
    geobject_transpose = geobject.transpose()
    number_object = list(geobject_transpose.keys())

    try :
        geobject_transpose.columns = geobject_transpose.loc['index']  #correct an indexing issue only on the massive polygon processing : polygons >  15000.
    except:
        pass

    for numberobj in number_object:
        #var = geobject_transpose.loc['var', numberobj]
        geobject_temp = pd.DataFrame(columns=['ID', 'DATE', var_out])
        geobject_temp[var_out] = geobject_transpose[numberobj]
        geobject_temp['ID'] = geobject_transpose.loc[vector_id, numberobj]
        geobject_temp['DATE'] = geobject.keys()
        geobject_temp = geobject_temp.drop(['geometry', vector_id, 'var'])

        geobject_rearrange = pd.concat([geobject_rearrange, geobject_temp])

    return geobject_rearrange