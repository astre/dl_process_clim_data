from src.era5_zonal_stats_multiproc import calculation_process_huge_polygon_multiproc, save_geobject

from src.tools.vector import boundingbox, open_vector, transform_projection
import pandas as pd
import geopandas as gpd
from src.tools.control_os import whatpathtype
from multiprocessing import freeze_support
from src.tools.meteofranceformat import save_txt_default, meteofrance_rearange_output

import time


whatpathtype()


def ERA5_dlandprocess(variables, starttime, endtime, path, vector_path, vector_column_to_keep, typedownload, progress_callback=None, **kargs):

    defaultKwargs = {'savefileSHP': False, 'savefileTXT': True, 'savefileTXT_Path': None}
    kargs = {**defaultKwargs, **kargs}

    stats = "mean"
    vector = open_vector(vector_path)
    vector = transform_projection(vector, 4326)
    bbox_vector = boundingbox(vector)

    # Initialize progress dictionary
    if progress_callback is not None:
        progress_callback({
            "total_download": 0, "current_download": 0,
            "calculation": 0, "calculation_total": len(variables), "save_file": 0, "save_total": 1, "processing_name":'Downloading products'
        })

    #download_all_data
    if typedownload == 'cdscopernicus':
        from src.downloads.era5_download_multithread import download_all_variables_data

        data_list = download_all_variables_data(variables, starttime, endtime, path, bbox_vector)
        if progress_callback:
            progress_callback({"total_download": len(variables), "current_download": len(variables)})
    elif typedownload  == 'gee':
        from src.downloads.era5_download_gee import download_gee_xr

        data_list = download_gee_xr(variables, starttime, endtime, bbox_vector)
        variables = list(data_list.keys())
        if progress_callback:
            progress_callback({"total_download": len(variables), "current_download": len(variables)})
    else:
        raise Exception("Not a valid Type Of Download, should be 'cdscopernicus' or 'gee'")


    output = pd.DataFrame()
    geobject = gpd.GeoDataFrame()

    #Loop on each variables selected
    for i, variable in enumerate(variables):
        # Update progress
        if progress_callback:
            progress_callback({
                "calculation": i + 1
            ,"processing_name": 'Extraction and calculation of ' + variable })

        #Open the file_list dictionary for the complete list of Posixpath of files previously selected

        geobject = calculation_process_huge_polygon_multiproc(vector, data_list[variable], stats,
                                                                 limit_polygon=15000, vector_column_to_keep=vector_column_to_keep)

        if kargs['savefileSHP'] == True:
            save_geobject(vector_path, geobject, stats)

        if kargs['savefileTXT'] == True:
            outrearrange = meteofrance_rearange_output(geobject, vector_column_to_keep, variable)

            try:
                output = pd.merge(output, outrearrange, how='inner')
            except:
                output = outrearrange

    print(progress_callback)

    if progress_callback:
        progress_callback({
            "processing_name": 'Saving product'
        })

    if kargs['savefileTXT'] == True:
        if kargs['savefileTXT_Path'] == None:
                save_txt_default(vector_path, output, stats)  # save the data in file
        else:
            output.to_csv(f"{kargs['savefileTXT_Path']}")

    if progress_callback:
        progress_callback({
            "save_file": 1, "processing_name": 'Product saved'
        })

    return geobject, output, progress_callback


#Windows protection for the multiprocessing
"""
The purpose of using if __name__ == '__main__': is to ensure that any code that follows this statement will only
be executed if the script is being run as the main program.
This is important when using multiprocessing because each process will import the module containing the multiprocessing
code, and you don't want the same code to be executed multiple times.
"""



if __name__ == '__main__':

    # Execution timer
    st = time.time()
    starttime = "2024-01-01"
    endtime = '2024-06-01'

    # variable to sudy, by default : ['t2m', 'tp']
    variables = ['t2m', 'tp']
    path = '/home/bouvier/Documents/GIT_projects/dl_process_clim_data/files_tests/era5_netcdf/'

    ### Shared object
    # vector_path = "/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/IRIS/CONTOUR-IRIS-WGS84.shp"
    #vector_path = "/home/bouvier/Documents/GIT_projects/dl_process_clim_data/files_tests/IRIS/CONTOUR-DEP.shp"
    # vector_path = "/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/IRIS/CONTOUR-IRIS-MTP_4326.shp"
    #vector_path = "/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/test_croatie/PARCELLES.shp"
    # vector_path = "/home/bouvier/Documents/GIT_projects/dl_process_clim_data/files_tests/RVF/reserve_faune.shp"
    # vector_path = "/home/bouvier/Bureau/emprise_france.shp"
    vector_path = "../data/Regions_BF.shp"


    ### Processing_elements
    vector_column_to_keep = "CHF_LIEU"
    #vector_column_to_keep = 'CODE_IRIS'
    #vector_column_to_keep = "NAS_MB"
    # vector_column_to_keep = 'code'

    typedownload = 'gee'
    #typedownload = 'cdscopernicus'

    freeze_support()


    geobject, output , progress_callback = ERA5_dlandprocess(variables, starttime, endtime, path, vector_path, vector_column_to_keep,
                                     typedownload)


# End Execution timer
    elapsed_time = time.time() - st
    print('Execution time:', time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))