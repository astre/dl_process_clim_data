from src.main import ERA5_dlandprocess
import traceback


if __name__ == '__main__':

    starttime = "2019-01-01"
    endtime = '2019-02-28'

    # variable to sudy, by default : ['t2m', 'tp']
    variables = ['t2m', 'tp']
    path = './tests/files_tests/era5_netcdf'

    ### Shared object
    vector_path = "./tests/files_tests/iris/CONTOUR-IRIS-MTP_4326.shp"

    ### Processing_elements
    vector_column_to_keep = 'CODE_IRIS'

    #typedownload = 'gee'
    typedownload = 'cdscopernicus'

    try :
        geobject, output = ERA5_dlandprocess(variables, starttime, endtime, path, vector_path, vector_column_to_keep, typedownload)

    except Exception:
        traceback.print_exc()

    #Analyze output to be sure....