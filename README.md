# The ERA5 tasks

What is ERA5 ?
ERA5 is a product from COPERNICUS initiative containing climatic data based one modeling and reanalysis (ground-based  station correction..).
The product used in the project is not ERA5 but the subproduct ERA5-Land.
The choice of this subproduct was defined by the spatio-temporal resolution (Native resolution at 9km, and one data per day) and the focus on the land surface exclusively.

The link on the official product : https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=overview

Edit : Since the CDS have issues, the download from the CDS is not relevant, it increases the download time to hours even for small files.
So, it's now possible to download files from the cloud https://developers.google.com/earth-engine/datasets/catalog/ECMWF_ERA5_LAND_DAILY_AGGR.

## Main goals

Product a task chain for :
- Automatically download the files with a spatio-temporal selection on cds-copernicus.
- Calculate mean / max /min of precipitation and temperature variable for each polygon inserted.

**Download based on multi-threading.**
**Calculs based on multiprocessing.**

## First configuration

### Copernicus data center
The download service is based on the cdsapi of Copernicus.

For a download execution you have to :

- sign in cds copernicus website at this adresse: https://cds.climate.copernicus.eu/.
- find the API located at the bottom of the *User Profile* on https://cds.climate.copernicus.eu/.
- create a file saved in your personal directory :

    - File name : 
            .cdsapirc
  
           Be carefull, with windows you have to define the name of the file as ".cdsapirc"
    - Contain : 

            url: https://cds.climate.copernicus.eu/api/v2
            key: yyyyy:xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxx
            verify: 0
  
  replace the factice api key by your own and save:

       The yyyyy is your UID and the xxxx is your API Key.
       Both of them can be found at the bottom of the 'User profile' page.

### Google Earth Engine

You have to create a classic Google email account or use an existing.
While the code is running the first time, you have to connect to the Google interface....
To complete... this part. 
Easy but don't remember the complete process.

## About Windows:

Due to the difference of multiprocessing tasks on windows (differ than linux), the processing is probably twice as slow.
Different approachs were tested but the time-consuming is longer than linux.
The explanation for this behavior can be consulted easily on internet.

In a nutshell : 
There are three multiprocessing methods, they are: spawn: start a new Python process. fork: copy a Python process from an existing process. forkserver: new process from which future forked processes will be copied.
Windows uses the multiprocessing methods named 'spawn' and linux can use spawn, but also fork and forkserver.
Actually fork is more efficiente than spawn : https://superfastpython.com/fork-faster-than-spawn/

Additionally, windows is slower than linux because of numbers of programmes runnings in back, and it's my case, i can't deactivate the antivirus because of my company.
The antivirus will check each new process on windows, and windows has to create each process, it can't be copied...

So, the best practice is to go on Linux... :)


## How

### It works

First steps :

- Create an account on cds.copernicus, and create the .cdsapirc file.

- Spatio-temporal selection : 
    - The user have to insert a vector file :
        - a shape (.shp) 
        - a geojson file

- Define a folder to save the Netcdfs from copernicus.

Second steps:

- Install all the packages from the file requirements.txt.
- Replace the package variables and path you want from variables at the end of the main file.
- Execute the file/functions from main file.

Third steps:

- The files are downloaded if they are not presents in the path your wrote. 
There is a control of file name : the file name is composed with the date and the bounding box or the geographical date.
- The calculation process begin, close all the others application.
- At the end, depending on your choice, you can find the vector with all the calculation for Precipitation and Temperature.
    In addition, it will be the same if you define the file output as a TXT file...

### Run the script

Execute the script **main.py** or import the function ERA5_dlandprocess from the script.

## Please, make sure to use a Virtual Environnement

Good practices with the package begin by using a virtual environnement for your code in python.
You can choose the environnement you want like venv or conda env.

Access to your virtual env and execute the script.



## ToDo list

- [X] Render the script windows friendly
- [ ] Manage multithreading to send queries to the server for both temp and prec on the same time.
- [ ] Allow user to save data from gee in netcdf type on local disk
- [ ] Improve comments in code
- [X] Create a stack of netcdf on the period selected before calculation.
- [ ] Download exclusively the period selected and not the entire month containing the first/last month.
- [ ] Add a .cdsapirc verification file.
- [ ] Develop a simple interface (React ? Dash ? Python Shiny ?) for communicate with the package.
- [ ] Compare the product with others (NRT products exclusively)
