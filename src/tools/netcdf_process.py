from pathlib import Path, PosixPath, WindowsPath
from src.tools.control_os import whatpathtype
import xarray as xr
import traceback

def list_netcdf(path):
    """
    @param path: the directory path of your raster file
    @param raster_files: control the file extension (should be modified by other extent).
    @return: a list of file in your directory
    """
    osdef = whatpathtype()
    files = list()

    if osdef == WindowsPath :
        p = Path(path)
        files = [x for x in p.iterdir() if x.is_file()]

    if osdef == PosixPath :
        p = Path(path).glob('*/')
        files = [x for x in p if x.is_file()]


    raster_files = list(filter(lambda x: ('.nc' in x._str), files))
    return raster_files

"""
def combine_netcdf(netcdffiles_list):

    nc_file = xr.merge([xr.open_dataset(f) for f in netcdffiles_list])

    return nc_file

    autre approche : 
def combine_netcdf(netcdffiles_list):

    Merge des netcdf

    def open_dataset_with_fallback(file, engines=("h5netcdf", "netcdf4")):
        print(f"merge started with {file}")
        for engine in engines:
            try:
                file_opened = xr.open_dataset(file, engine=engine)
                file_opened = file_opened.astype('float32') #optimisation memory use
                return file_opened
            except Exception as e:
                print(f"Failed to open {file} with engine {engine}: {e}")
        raise RuntimeError(f"Unable to open {file} with any of the specified engines: {engines}")


    nc_file = xr.merge([open_dataset_with_fallback(f) for f in netcdffiles_list])
    print("merge finalized")
    return nc_file
"""


def open_dataset_with_fallback(file, engines=("h5netcdf", "netcdf4")):
    """
    Try to open a NetCDF file with multiple engines.

    Parameters:
        file (str): Path to the NetCDF file.
        engines (tuple): Tuple of engines to try for opening the file.

    Returns:
        xarray.Dataset: Opened dataset.
    """
    for engine in engines:
        try:
            print(f"Opening {file} with engine '{engine}'")
            dataset = xr.open_dataset(file, engine=engine)
            return dataset.astype("float32")  # Convert to float32 for memory optimization
        except Exception as e:
            print(f"Failed to open {file} with engine '{engine}': {e}")
    raise RuntimeError(f"Unable to open {file} with any of the specified engines: {engines}")

def combine_netcdf(netcdffiles_list):
    """
    Merge multiple NetCDF files into a single xarray dataset, trying each file with 'h5netcdf' or 'netcdf4'.

    Parameters:
        netcdffiles_list (list): List of NetCDF file paths to merge.

    Returns:
        xarray.Dataset: Merged dataset.
    """

    # Open each file with fallback and store the datasets in a list
    opened_datasets = []
    for file in netcdffiles_list:
        ds = open_dataset_with_fallback(file)
        opened_datasets.append(ds)

    # Merge all datasets
    merged_dataset = xr.merge(opened_datasets)
    print("Merge completed successfully.")
    return merged_dataset


def control_obj_netcdf(netcdfobj):
    #Test if it's a path or a list of path in PosixPath format
    if type(netcdfobj) == type(str()):

        try :
            ncfiles = list_netcdf(netcdfobj)

        except Exception:
            return traceback.print_exc()

    elif type(netcdfobj) == type(list()):

        typepath = whatpathtype()

        if type(netcdfobj[0]) == typepath:
            total_size = 0
            for file_path in netcdfobj:
                try:
                    size = Path(file_path).stat().st_size
                    total_size += size
                except FileNotFoundError:
                    print(f"{file_path}: File not found")
                except Exception as e:
                    print(f"{file_path}: Error: {str(e)}")

            if total_size < 400000000 : #if total size of files are under 40mb, combine them, if upper, not, the process will be a loop

                ncfiles = [combine_netcdf(netcdfobj)] #A retirer si prend top de mémoire de stocker tous les fichiers necdf ensemble

            else:
                ncfiles = netcdfobj
        else:
            raise TypeError (print('The list entry is not in PosixPath format (package pathlib)'))

    elif type(netcdfobj) == xr.Dataset:
            ncfiles = [netcdfobj]

    else :
        raise TypeError (print('The "ntcdfpath" parameter must be a path to your folder or the list of your nectdf file in PosixPath format (package pathlib)'))

    return ncfiles

#def save_netcdf_object():
