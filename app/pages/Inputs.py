from datetime import date
import dash
from dash import dcc, html, callback, Output, Input,State, dash_table, no_update
import dash_bootstrap_components as dbc
import dash_leaflet as dl
import base64
import os
import json
import geopandas as gpd
import pandas as pd
from xarray.core.computation import result_name

from src.main import ERA5_dlandprocess
from pathlib import Path
import threading
from app.cache import cached_store_data, get_cached_data


# To create meta tag for each page, define the title, image, and description.
dash.register_page(__name__,
                   path='/inputs',  # '/' is home page and it represents the url
                   name='Inputs',  # name of page, commonly used as name of link
                   title='Inputs',  # title that appears on browser's tab
                   #image='pg1.png',  # image in the assets folder
                   description='Inputs data for the processing'
)

#---- Map parameters --------
upload_directory = Path(__file__).resolve().parent.parent.parent / 'data'

df = pd.DataFrame({"No_Data": [0]})

#------------

# Define the layout
layout = html.Div([
  # Store for selected column
        dcc.Store(id='progress-store', storage_type='session'),
    dbc.Row([
        # Column for the input parameters
        dbc.Col([
            html.H1('Properties selection'),
            html.Br(),
            html.H4('Date range'),
            dcc.DatePickerRange(
                display_format='Y/M/D',
                id='my-date-picker-range',
                min_date_allowed=date(1990, 8, 5),
                max_date_allowed=date.today(),
                initial_visible_month=date.today(),
                start_date=date.today(),
                end_date=date.today(),
                style={'zIndex': 1000, 'position': 'relative'}  # Ensure it appears above the map
            ),
            html.Div(id='output-container-date-picker-range'),
            html.Br(),
            html.H4("Data provider:"),
            dcc.Dropdown(
                id="dropdown_provider",
                options=[
                    {"label": "Google Earth Engine", "value": "gee"},
                    {"label": "Copernicus", "value": "cdscopernicus"}
                ],
                value="gee",
                clearable=False,
            ),
            html.Br(),
            html.H4("Available variables:"),
            dcc.Dropdown(
                id="bar-polar-app-x-dropdown",
                value=["tp", "t2m"],
                options=[
                    {"label": "Total precipitation", "value": "tp"},
                    {"label": "Temperature", "value": "t2m"}
                ],
                multi=True,
            ),
            html.Br(),
            html.H4("Insert your geographical data"),
            dcc.Upload(
                id='file-upload',
                children=dbc.Button("Upload Shapefile (.shp, .shx, .dbf)"),
                multiple=True
            ),
            html.Div(
                id='file-upload-status',
                style={'marginTop': '10px', 'color': 'blue'}
            ),
            html.Br(),
            html.H5("Select a column with unique IDs"),
            html.Label("Choose a column:"),
            dcc.Dropdown(
                id="column-selector",
                options=[{"label": col, "value": col} for col in df.columns],
                value=df.columns[0],  # Default value
            ),
            html.Div(id='data-preview', style={"marginTop": "20px"})
        ], width=3)  # Adjust the width as needed (e.g., 6 out of 12 columns)
    ,

        # Column for the map
        dbc.Col([
            dl.Map(
                [
                    dl.TileLayer(),
                    dl.GeoJSON(
                        id='geojson-layer',
                        data={
                            "type": "FeatureCollection",
                                "features": []
                            },
                        style={
                            'weight': 1,
                            'fillColor': 'blue',
                            'color': 'blue',
                            'fillOpacity': 0.5
                        },
                        zoomToBounds=True ,
                    )
                ],
                id='shared-viewport',
                center=[46.0, 2.0],  # Center for France
                zoom=5,  # Default zoom level
                style={'width': '100%', 'height': '600px', 'zIndex': 500}  # Set a lower z-index for the map
            ),


            # Button, progress bar, and status under the map
            html.Div([
                dbc.Button("Start processing", id="process-button", color="primary", style={"marginTop": "20px", "textAlign": "center", "padding": "20px", "marginLeft": "auto","marginRight": "auto", "display": "block"}),
                dcc.Interval(id='interval-component', interval=1 * 1000, n_intervals=0),  # Update every second
                dbc.Progress(id="progress-bar", value=0, striped=True, animated=True, style={"marginTop": "20px"}),
                html.Div(id='processing-status', style={"textAlign": "center", "marginTop": "20px"}),
                html.Div(id='output')
            ])
        ], width=9),  # Adjust the width as needed (e.g., 6 out of 12 columns)
        ])
])







#------------Select Range Date ----------------
# Initial callback to store the selected dates
# Callback to initialize the DatePickerRange component with stored dates
@callback(
    Output('my-date-picker-range', 'start_date'),
    Output('my-date-picker-range', 'end_date'),
    Input('init-date-store', 'data'), #to prevent reinitialization after the first run / it's empty and normal ....
    State('date-store', 'data')
)
def initialize_date_picker(init_data, stored_dates):
    if stored_dates is not None and init_data is None:
        return stored_dates['start_date'], stored_dates['end_date']
    return no_update, no_update

# Separate callback to store the selected dates in dcc.Store
@callback(
    Output('date-store', 'data'),
    Input('my-date-picker-range', 'start_date'),
    Input('my-date-picker-range', 'end_date')
)
def store_dates(start_date, end_date):
    today = date.today().isoformat()

    if start_date and end_date and (start_date != today or end_date != today):
        return {'start_date': start_date, 'end_date': end_date}

    return no_update

# Callback to update the output container based on stored dates
@callback(
    Output('output-container-date-picker-range', 'children'),
    Input('date-store', 'data')
)
def update_output(data):
    string_prefix = 'You have selected: '
    if data is not None:
        start_date = data.get('start_date')
        end_date = data.get('end_date')

        if start_date is not None:
            start_date_object = date.fromisoformat(start_date)
            start_date_string = start_date_object.strftime('%d, %B, %Y')
            string_prefix = string_prefix + 'Start Date: ' + start_date_string + ' | '
        if end_date is not None:
            end_date_object = date.fromisoformat(end_date)
            end_date_string = end_date_object.strftime('%d, %B, %Y')
            string_prefix = string_prefix + 'End Date: ' + end_date_string
    if len(string_prefix) == len('You have selected: '):
        return 'Select a date to see it displayed here'
    else:
        return string_prefix

#------------
#------------

# Callback to handle data provider selection and storage
@callback(
    Output('dropdown_provider', 'value', allow_duplicate=True),
    Output('dataprovider', 'data'),
    Input('dropdown_provider', 'value'),
    State('dataprovider', 'data'),
    prevent_initial_call=True
)
def manage_provider(selected_provider, stored_provider):
    ctx = dash.callback_context

    if selected_provider is not None:
        return selected_provider, selected_provider

    return stored_provider if stored_provider else "gee", stored_provider


# Callback to initialize provider value on page load
@callback(
    Output('dropdown_provider', 'value'),
    Input('dataprovider', 'data'),
)
def init_provider(stored_provider):
    return stored_provider if stored_provider else "gee"


# Callback to handle variables selection and storage
@callback(
    Output('bar-polar-app-x-dropdown', 'value', allow_duplicate=True),
    Output('selected-variables', 'data'),
    Input('bar-polar-app-x-dropdown', 'value'),
    State('selected-variables', 'data'),
    prevent_initial_call=True  # Set to True as required for allow_duplicate
)
def manage_variables(selected_vars, stored_vars):
    ctx = dash.callback_context

    if selected_vars is not None:
        return selected_vars, selected_vars

    return stored_vars if stored_vars else ["tp", "t2m"], stored_vars


# Callback to initialize variables value on page load
@callback(
    Output('bar-polar-app-x-dropdown', 'value'),
    Input('selected-variables', 'data'),
)
def init_variables(stored_vars):
    return stored_vars if stored_vars else ["tp", "t2m"]

#------------
#------------

#------------ shape file ------------

# ------------ shape file ------------
@callback(
    Output('geojson-layer', 'data'),
    Output('shared-viewport', 'data'),
    Output('file-upload-status', 'children'),
    Output('uploaded-data', 'data'),
    Output('df-columns', 'data'),
    Output('column-selector', 'value'),
    Output('path-to-vector', 'data'),
    Output('test_dict_sharing', 'data'),
    Input('file-upload', 'contents'),
    State('file-upload', 'filename'),
    State('geojson-layer', 'data'),
    State('shared-viewport', 'data'),
    State('file-upload-status', 'children'),
    State('uploaded-data', 'data'),
    State('df-columns', 'data'),
    State('column-selector', 'value'),
    State('path-to-vector', 'data'),
    State('test_dict_sharing', 'data'),
    prevent_initial_call=True
)
def handle_file_upload(contents, filenames, current_geojson, current_viewport, current_status, current_uploaded_data, current_df_columns, current_column_selector, current_path_to_vector, test_dict_sharing):
    # Initialize test_dict_sharing if None
    if test_dict_sharing is None:
        test_dict_sharing = {}

    # Update current_viewport from test_dict_sharing
    current_viewport = test_dict_sharing
    print(test_dict_sharing)

    # If no contents are uploaded, check and return cached data
    if not contents:
        cached_data = get_cached_data("geodatauploaded")
        if cached_data:
            return cached_data["geodatauploaded"], current_viewport, current_status, current_uploaded_data, current_df_columns, current_column_selector, current_path_to_vector, test_dict_sharing
        return current_geojson, current_viewport, current_status, current_uploaded_data, current_df_columns, current_column_selector, current_path_to_vector, test_dict_sharing

    try:
        required_extensions = {'.shp', '.shx', '.dbf'}
        uploaded_files = {os.path.splitext(f)[1].lower() for f in filenames}

        # Check if all required files are uploaded
        if not required_extensions.issubset(uploaded_files):
            return current_geojson, current_viewport, "Please upload .shp, .shx, and .dbf files.", current_uploaded_data, current_df_columns, current_column_selector, current_path_to_vector, test_dict_sharing

        # Save uploaded files
        saved_files = {}
        for content, filename in zip(contents, filenames):
            content_type, content_string = content.split(',')
            decoded = base64.b64decode(content_string)
            file_path = os.path.join(upload_directory, filename)

            with open(file_path, 'wb') as f:
                f.write(decoded)
            saved_files[os.path.splitext(filename)[1].lower()] = file_path

        # Read the shapefile
        gdf = gpd.read_file(saved_files['.shp'])

        # Check for empty or invalid geometries
        if gdf.empty or gdf.geometry.is_empty.all():
            return current_geojson, current_viewport, "Shapefile is empty or contains invalid geometries.", current_uploaded_data, current_df_columns, current_column_selector, current_path_to_vector, test_dict_sharing

        # Transform CRS to EPSG:4326 if necessary
        if gdf.crs != "EPSG:4326":
            gdf = gdf.to_crs("EPSG:4326")

        # Calculate bounds and center for the viewport
        bounds = gdf.total_bounds
        minx, miny, maxx, maxy = bounds
        center = [(miny + maxy) / 2, (minx + maxx) / 2]
        center = [float(coord) for coord in center]
        zoom = 15

        # Convert GeoDataFrame to GeoJSON
        geojson_data = json.loads(gdf.to_json())

        # Prepare viewport data
        viewport_data = {
            "center": center,
            "zoom": zoom,
            "transition": "zoomOnEntities"
        }

        # Prepare DataFrame for columns
        df = pd.DataFrame(gdf.drop(columns='geometry'))
        first_column = df.columns[0] if not df.empty else None

        # Update test_dict_sharing with viewport data
        test_dict_sharing = dict(viewport_data)

        # Debugging information
        print("Path to vector:", saved_files['.shp'])
        print("Viewport from inputs:", viewport_data)

        # Cache the geojson_uploaded data
        cached_data = cached_store_data('geodatauploaded', {'geodatauploaded':geojson_data})

        #test = cached_store_data(geojson_data)

        return cached_data['geodatauploaded'], viewport_data, "Shapefile loaded successfully!", df.to_dict('records'), df.columns.tolist(), first_column, saved_files['.shp'], test_dict_sharing

    except Exception as e:
        return current_geojson, current_viewport, f"Error processing files: {str(e)}", current_uploaded_data, current_df_columns, current_column_selector, current_path_to_vector, test_dict_sharing


    # ------------ preview id column ------------


# Callback to update the column selector options and set the stored value
@callback(
    Output("column-selector", "options"),
    Output("column-selector", "value", allow_duplicate=True),
    Input("df-columns", "data"),
    State("column-store", "data"),
    prevent_initial_call=True
)
def update_column_selector_options(columns, stored_column):
    if columns is None:
        return [], no_update

    options = [{"label": col, "value": col} for col in columns]

    # If we have a stored column and it's in the available columns, select it
    if stored_column and stored_column in columns:
        return options, stored_column

    # Otherwise, select the first column if available
    return options, columns[0] if columns else no_update


# Callback to store the selected column
@callback(
    Output('selected-column', 'data'),
    Output('column-store', 'data'),
    Input('column-selector', 'value'),
    State('column-store', 'data'),
    prevent_initial_call=True
)
def store_selected_column(selected_column, stored_column):
    if selected_column is not None and selected_column != "No_Data":
        return selected_column, selected_column
    return no_update, stored_column


# Callback to update the data preview
@callback(
    Output("data-preview", "children"),
    [Input("column-selector", "value"),
     Input('uploaded-data', 'data')],
    prevent_initial_call=True
)
def update_preview(selected_column, data):
    if data is None or selected_column is None:
        return html.Div("No data available")

    # Convert the stored data back to a DataFrame
    df = pd.DataFrame(data)

    # Check if the selected column exists in the DataFrame
    if selected_column not in df.columns:
        return html.Div(f"Column '{selected_column}' does not exist in the data")

    # Get a preview of the selected column (first 2 rows)
    preview_df = pd.DataFrame({selected_column: df[selected_column].head(2)})

    # Create a Dash DataTable
    preview_table = dash_table.DataTable(
        data=preview_df.to_dict("records"),
        columns=[{"name": col, "id": col} for col in preview_df.columns],
        style_table={
            "overflowX": "auto",
            "width": "200px",  # Adjust the width as needed
            "height": "200px"  # Adjust the height as needed
        },
        style_cell={"textAlign": "center", "padding": "5px"},
        style_header={"backgroundColor": "lightgrey", "fontWeight": "bold"}
    )
    return preview_table

#------------
#------------


#------------ processing ------------
def progress_callback(progress_update, progress_store):
    # Update the progress store
    progress_store.update(progress_update)
    # Dump the updated progress to a JSON file
    with open('progress.json', 'w') as f:
        json.dump(progress_store, f)


# Define current_thread and current_result at the module level (top of your file)
current_thread = None
current_result = None


class ThreadWithReturn(threading.Thread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.result = None

    def run(self):
        if self._target is not None:
            self.result = self._target(*self._args, **self._kwargs)

# Callback to start processing
@callback(
    Output('output', 'children'),
    Output('progress-store', 'data'),
    Input('process-button', 'n_clicks'),
    State('selected-variables', 'data'),
    State('selected-column', 'data'),
    State('date-store', 'data'),
    State('path-to-vector', 'data'),
    State('dataprovider', 'data'),
    prevent_initial_call=True
)
def start_processing(n_clicks, selected_variables, selected_column, date_store, path_to_vector, data_provider):
    if n_clicks > 0:
        # Declare we're using the global variable
        global current_thread

        # Initialize progress store
        progress_store = {
            "total_download": 0, "current_download": 0,
            "calculation": 0, "calculation_total": 0,
            "save_file": 0, "save_total": 0, "processing_name": "Processing..."
        }

        def progress_callback_wrapper(progress_update):
            progress_callback(progress_update, progress_store)

        # Create thread with return value
        current_thread = ThreadWithReturn(target=ERA5_dlandprocess, args=(
            selected_variables, date_store['start_date'], date_store['end_date'],
            upload_directory, path_to_vector, selected_column, data_provider, progress_callback_wrapper
        ))
        current_thread.start()

        # Save the initial progress to a JSON file
        with open('progress.json', 'w') as f:
            json.dump(progress_store, f)

        return None, progress_store

    return "No result yet", None


@callback(
    Output("result-store-geo", "data"),
    Output("result-store-data", "data"),
    Input("interval-component", "n_intervals"),
    State("result-store-geo", "data"),
    prevent_initial_call=True
)
def check_result(n, state_result):
    if state_result is None:
        global current_thread
        if current_thread and not current_thread.is_alive():
            result = current_thread.result
            current_thread = None  # Reset the thread
            geodata_json = json.loads(result[0].to_json())
            data_json = json.loads(result[1].to_json())
            return geodata_json, data_json
    return dash.no_update

#------------

@callback(
    Output('progress-bar', 'value'),
    Output('processing-status', 'children'),
    Input("interval-component", "n_intervals"),
)
def update_progress_bar(n_intervals):
    # Load the progress from the JSON file
    try:
        with open('progress.json', 'r') as f:
            progress = json.load(f)
    except FileNotFoundError:
        progress = None

    if progress is None:
        return 0

    # Calculate the overall progress as a percentage
    total_steps = progress.get("total_download", 0) + progress.get("calculation_total", 0) + progress.get("save_total", 0)
    if total_steps == 0:
        return 0, "Not Started"  # Avoid division by zero
    current_steps = progress.get("current_download", 0) + progress.get("calculation", 0) + progress.get("save_file", 0)
    processing_name = progress.get("processing_name", "Processing...")

    return int((current_steps / total_steps) * 100), processing_name


#take all data to be stored in main to be accessed by other pages


