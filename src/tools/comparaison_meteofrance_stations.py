import pandas as pd
import geopandas as gpd


stations = pd.read_csv('/home/bouvier/Documents/Dengue_modeling/stations.txt')
data_stations = pd.read_csv('/home/bouvier/Documents/Dengue_modeling/407stations_20210101_20221130.txt')

vector = gpd.read_file("/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/IRIS/tp_mean_CONTOUR-IRIS-MTP_4326.shp")

model_villevielle = vector[vector['CODE_IRIS'] == '303520000']

station_villevielle = data_stations[data_stations["POSTE"] == 30352002]
station_villevielle = station_villevielle[['DATE', 'RR']]
station_villevielle['DATE'] = station_villevielle['DATE'].map(str)
station_villevielle['DATE'] = station_villevielle['DATE'].str.slice(0,4)+str('-')+station_villevielle['DATE'].str.slice(4,6)+str('-')+station_villevielle['DATE'].str.slice(6,8)
station_villevielle = station_villevielle.transpose()
station_villevielle  = station_villevielle.set_axis(station_villevielle.loc['DATE',:], axis='columns')
station_villevielle = station_villevielle.drop("DATE")

test = pd.merge(model_villevielle, station_villevielle, how='outer')
test = test.drop('CODE_IRIS', axis=1)
test = test.drop('geometry', axis=1)
test2 = test.transpose()
test2 = test2.sort_index()
test2 = test2.drop('var')
test2.plot()




data_stations = pd.read_csv('/home/bouvier/Documents/Dengue_modeling/407stations_20210101_20221130.txt')

vector = gpd.read_file("/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/IRIS/t2m_mean_CONTOUR-IRIS-MTP_4326.shp")

model_villevielle = vector[vector['CODE_IRIS'] == '303520000']

station_villevielle = data_stations[data_stations["POSTE"] == 30352002]
station_villevielle['TP'] = (station_villevielle.loc[:,'TN'] + station_villevielle.loc[:,'TX']) / 2
station_villevielle = station_villevielle[['DATE', 'TP']]
station_villevielle['DATE'] = station_villevielle['DATE'].map(str)
station_villevielle['DATE'] = station_villevielle['DATE'].str.slice(0,4)+str('-')+station_villevielle['DATE'].str.slice(4,6)+str('-')+station_villevielle['DATE'].str.slice(6,8)
station_villevielle = station_villevielle.transpose()
station_villevielle  = station_villevielle.set_axis(station_villevielle.loc['DATE',:], axis='columns')
station_villevielle = station_villevielle.drop("DATE")

test = pd.merge(model_villevielle, station_villevielle, how='outer')
test = test.drop('CODE_IRIS', axis=1)
test = test.drop('geometry', axis=1)
test2 = test.transpose()
test2 = test2.sort_index()
test2 = test2.drop('var')
test2.plot()