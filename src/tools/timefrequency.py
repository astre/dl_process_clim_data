import pandas as pd

def findtimefrequency(netcdf_time_object):

    # Convert time to pandas datetime index
    time_index = pd.to_datetime(netcdf_time_object.values)
    # Calculate the differences between consecutive time points
    time_diffs = time_index[1:] - time_index[:-1]

    # Get the most common frequency
    frequency = pd.infer_freq(time_index)

    print(f"The inferred frequency is: {frequency}")

    # If pd.infer_freq returns None, it means it couldn't infer a single frequency,
    # in that case, you can manually inspect the time_diffs
    if frequency is None:
        print("Could not infer a single frequency. Here are the time differences:")
        print(time_diffs)

    return frequency