from cachetools import cached, TTLCache
import json
import uuid

# Create a TTLCache instance
cache = TTLCache(maxsize=10000, ttl=300000000)
latest_keys = {}  # Dictionary to store the latest key for each prefix

def generate_unique_key(prefix):
    unique_id = uuid.uuid4()
    return f"{prefix}_{unique_id}"

def cached_store_data(prefix, data):
    unique_key = generate_unique_key(prefix)
    serialized_key = json.dumps(unique_key, sort_keys=True)
    cache[serialized_key] = data
    latest_keys[prefix] = unique_key  # Store the latest unique key for the prefix
    supr_previous_cache(serialized_key, prefix)    #supprime le cache pour pas se le trimbaler si update sur de nombreuses zones.
    return data  # Return the unique key for future retrieval

def get_cached_data(prefix):
    unique_key = latest_keys.get(prefix)
    try:
        return cache[json.dumps(unique_key, sort_keys=True)]
    except:
        return None

def supr_previous_cache(serialized_key, prefix):
    keys = cache._Cache__data.keys()
    keylist = []
    for key in keys:
        keylist.append(key)
    for key in keylist:
        if prefix in key:
            if key != serialized_key :
                del cache[key]


def get_all_cached_data():
    return {json.loads(key): value for key, value in cache.items()}